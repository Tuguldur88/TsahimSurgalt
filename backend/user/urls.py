from django.urls import path

from . import views

urlpatterns = [
    path('postRegExample', views.postRegExample, name='postRegExample'),
    path('getRegExample', views.getRegExample, name='getRegExample'),
]